Feature('Auto Login');


Scenario('log me in', (I, login) => {
    login('admin');
    I.wait(5);
    //halo selamat datang
    //click agenda
    I.click('.ui.orange.large.pointing div .item:nth-child(2) .title');
    //click buat baru
    I.click('.ui.orange.large.pointing div .item:nth-child(2) .content.active .menu.accordionMenu .item:nth-child(3)');
    //input title
    I.fillField('.main-content div .ui.stackable.grid .row:nth-child(2) .ten.wide .ui.form .field .ui.fluid input[name=title]','testing judul');
    //input content
    I.appendField('div[role=textbox]', 'testing content');
    I.pressKey('Enter');
    I.appendField('div[role=textbox]', 'lorem ipsum lorem ipsum');
    //marker map
    I.click('.gmnoprint:nth-child(11) div:nth-child(2) div[role=button]');
    I.wait(5);
    I.click('div.ui.segments .ui.segment:nth-child(3) div[style] div[tabindex]');
    I.scrollTo('.ui.segments .ui.segment:nth-child(3)');
    I.wait(5);
    //input gambar
    I.scrollPageToTop();
    I.attachFile('.ui.stackable .row:nth-child(2) .six.wide .ui.segments .field.inputfile .ui.input input[type=file]', 'images/sample-image.png');
    I.wait(5);
    //pilih penting
    I.click('div.field div[role=listbox]');
    I.click('.ui.stackable .row:nth-child(2) .six.wide .ui.form .inline.fields .field div[role=listbox] .menu.transition div[role=option]:nth-child(2)');
    I.wait(3);
    //click preview
    I.click('.ui.stackable .row:nth-child(2) .six.wide .ui.form button');
    I.wait(3);
    //click kirim
    I.click('.ui.grid .centered.row:nth-child(3) .seven.wide.column .ui.segments .ui.clearing.segment .ui.green.right');
    I.wait(5);
 });