Feature('Login');

let account = new DataTable(['username', 'password']);
account.add(['superadm', 'test123']);
account.add(['test123', 'test123']);

let url = "https://jaki-dev.jakarta.go.id/admin#/";

// versi pertama data driven
Data(account).Scenario('test something', (I, current) => {
    I.amOnPage(url);
    I.wait(5);
    //I.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]','superadm');
    //I.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]','test123');
    // Pass dataTable to Data()
    // Use special param `current` to get current data set
    I.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]', current.username);
    I.fillField('.ui.form .field:nth-child(2) .ui.big input[name=password]', current.password);
    I.click('.ui.form .field:nth-child(3) .ui.big');
    I.wait(10);
    I.see('Selamat Datang di JAKI Web Admin');
    I.wait(5);
});

// versi kedua data driven
Data(account).only.Scenario('test something', (I, current) => {
    I.amOnPage(url);
    I.wait(5);
    //I.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]','superadm');
    //I.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]','test123');
    // Pass dataTable to Data()
    // Use special param `current` to get current data set
    I.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]', current.username);
    I.fillField('.ui.form .field:nth-child(2) .ui.big input[name=password]', current.password);
    I.click('.ui.form .field:nth-child(3) .ui.big');
    I.wait(10);
    I.see('Selamat Datang di JAKI Web Admin');
    I.wait(5);
});

//versi ketiga data driven
Data(account.filter(account => account.username == 'superadm')).Scenario('test something', (I, current) => {
    I.amOnPage(url);
    I.wait(5);
    //I.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]','superadm');
    //I.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]','test123');
    // Pass dataTable to Data()
    // Use special param `current` to get current data set
    I.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]', current.username);
    I.fillField('.ui.form .field:nth-child(2) .ui.big input[name=password]', current.password);
    I.click('.ui.form .field:nth-child(3) .ui.big');
    I.wait(10);
    I.see('Selamat Datang di JAKI Web Admin');
    I.wait(5);
});