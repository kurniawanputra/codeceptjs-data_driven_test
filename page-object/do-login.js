module.exports = function() {
    return actor({
  
      loginUser: function(username, password) {
        this.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]', username);
        this.fillField('.ui.form .field:nth-child(2) .ui.big input[name=password]', password);
        this.click('.ui.form .field:nth-child(3) .ui.big');
      }
    });
  }