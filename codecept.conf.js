exports.config = {
  tests: './test/**/*_test.js',
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://localhost',
      show: true,
      windowSize: "1180x960",
      chrome: {
        defaultViewport: {
          width: 1380,
          height: 960
        },
        disableScreenshots: false,
        args: [
          "--incognito",
          "--disable-infobars",
          "--windows-position=0,0",
          "--window-size=1180,960"
        ]
      }
    }
  },
  plugins: {
    autoLogin: {
      enabled: true,
      saveToFile: true,
      inject: 'login', // use `loginAs` instead of login
      users: {
        admin: {
          login: (I) => {
            I.amOnPage("https://jaki-dev.jakarta.go.id/admin#/");
            I.wait(5);
            I.fillField('.ui.form .field:nth-child(1) .ui.big input[name=username]', 'superadm');
            I.fillField('.ui.form .field:nth-child(2) .ui.big input[name=password]', 'superadm');
            I.click('.ui.form .field:nth-child(3) .ui.big');
          },
          check: (I) => {
            I.amOnPage('https://jaki-dev.jakarta.go.id/admin#/admin/');
            I.see('Selamat Datang di JAKI Web Admin');
          },
        },
      }
    },
  },
  include: {
    I: './steps_file.js',
    DoLogin: './page-object/do-login.js'
  },
  bootstrap: null,
  mocha: {
    "reporterOptions": {
        "reportDir": "output"
    }
  },
  name: 'codeceptjs - data driven test'
}